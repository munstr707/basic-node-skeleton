# Basic Node Sekelton

Generates a basic node skeleton with minimal boilerplate.

## To use

- Clone repository then run `npm i -g` to install package globally on machine
- run `basic-node-proj-generator` in the directory you want to install the project
- follow the instructions
