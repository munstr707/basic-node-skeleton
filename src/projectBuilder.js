const fs = require("fs");
const { exec } = require("child_process");

const twirlLoadingAnimationWithMessage = require("./terminalUtils.js")
  .twirlLoadingAnimationWithMessage;

const CURRENT_DIRECTORY = process.cwd();

const createDirectoryContents = ({ templatePath, newProjectPath }) => {
  const filesToCreate = fs.readdirSync(templatePath);

  filesToCreate.forEach((fileName) => {
    const originalFilePath = `${templatePath}/${fileName}`;
    const currentFileStats = fs.statSync(originalFilePath);
    if (currentFileStats.isFile()) {
      const contents = fs.readFileSync(originalFilePath, "utf8");
      const correctedFileName =
        fileName === ".npmignore" ? ".gitignore" : fileName;
      const writePath = `${CURRENT_DIRECTORY}/${newProjectPath}/${correctedFileName}`;
      fs.writeFileSync(writePath, contents, "utf8");
    } else if (currentFileStats.isDirectory()) {
      fs.mkdirSync(`${CURRENT_DIRECTORY}/${newProjectPath}/${fileName}`);
      createDirectoryContents({
        templatePath: `${templatePath}/${fileName}`,
        newProjectPath: `${newProjectPath}/${fileName}`,
      });
    }
  });
};

const installPackages = () => {
  return new Promise((resolve) => {
    const installingPackagesAnimation = twirlLoadingAnimationWithMessage(
      "Installing Package"
    );
    exec("npm i", (error, stdout, stderr) => {
      clearInterval(installingPackagesAnimation);
      console.log(stderr ? `\n${stderr}\n`.red : "");
      console.log(error ? `\n${error}\n`.red : "");
      console.log(`\n${stdout}\n`.green);
      console.log("\nFinished installing packages\n".green);
      resolve();
    });
  });
};

const cdIntoNewApp = process.chdir;

const buildProjectFromAnswers = ({ templateProjectPath }) => async (
  answers
) => {
  const projectName = answers["project-name"];
  fs.mkdirSync(`${CURRENT_DIRECTORY}/${projectName}`);
  createDirectoryContents({
    templatePath: templateProjectPath,
    newProjectPath: projectName,
  });
  cdIntoNewApp(projectName);
  await installPackages();
  console.log("ALL FINISHED!!! HAPPY HACKING :D".green);
};

module.exports = buildProjectFromAnswers;
